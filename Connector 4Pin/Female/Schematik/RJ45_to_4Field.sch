EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RJ45 J1
U 1 1 5BB2295E
P 3800 1650
F 0 "J1" H 4000 2150 50  0000 C CNN
F 1 "RJ45" H 3650 2150 50  0000 C CNN
F 2 "Connectors:RJ45_SMT" H 3800 1650 50  0001 C CNN
F 3 "" H 3800 1650 50  0001 C CNN
	1    3800 1650
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x04 J2
U 1 1 5BB229B1
P 4750 2550
F 0 "J2" H 4750 2750 50  0000 C CNN
F 1 "4Pin_Feld" H 4750 2250 50  0000 C CNN
F 2 "Connectors:4Pin_Field" H 4750 2550 50  0001 C CNN
F 3 "" H 4750 2550 50  0001 C CNN
	1    4750 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2450 3450 2450
Wire Wire Line
	3450 2450 3450 2100
Wire Wire Line
	3550 2100 3550 2550
Wire Wire Line
	3550 2550 4550 2550
Wire Wire Line
	3650 2650 4550 2650
Wire Wire Line
	3650 2650 3650 2100
Wire Wire Line
	4550 2750 3750 2750
Wire Wire Line
	3750 2750 3750 2100
NoConn ~ 4350 1300
NoConn ~ 3850 2100
NoConn ~ 4050 2100
NoConn ~ 3950 2100
NoConn ~ 4150 2100
$EndSCHEMATC
