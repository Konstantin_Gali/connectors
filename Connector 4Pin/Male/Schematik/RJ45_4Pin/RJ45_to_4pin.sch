EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:RJ45_to_4pin-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x04 J2
U 1 1 5B802011
P 5800 3100
F 0 "J2" H 5800 3300 50  0000 C CNN
F 1 "Conn_01x04" H 5800 2800 50  0000 C CNN
F 2 "Connectors:RoundConnector_4pin" H 5800 3100 50  0001 C CNN
F 3 "" H 5800 3100 50  0001 C CNN
	1    5800 3100
	1    0    0    -1  
$EndComp
$Comp
L RJ45 J1
U 1 1 5B84FEDA
P 4350 2450
F 0 "J1" H 4550 2950 50  0000 C CNN
F 1 "RJ45" H 4200 2950 50  0000 C CNN
F 2 "Connectors:RJ45_SMT" H 4350 2450 50  0001 C CNN
F 3 "" H 4350 2450 50  0001 C CNN
	1    4350 2450
	1    0    0    -1  
$EndComp
NoConn ~ 4900 2100
NoConn ~ 4700 2900
NoConn ~ 4600 2900
NoConn ~ 4500 2900
NoConn ~ 4400 2900
Wire Wire Line
	4000 3000 5600 3000
Wire Wire Line
	4100 3100 5600 3100
Wire Wire Line
	4200 3200 5600 3200
Wire Wire Line
	4300 3300 5600 3300
Wire Wire Line
	4000 3000 4000 2900
Wire Wire Line
	4100 2900 4100 3100
Wire Wire Line
	4200 3200 4200 2900
Wire Wire Line
	4300 3300 4300 2900
$EndSCHEMATC
